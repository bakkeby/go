# Go

Go is a bash + Tcl solution to make directory changes faster by allowing you to "bookmark" often visited directories and quickly jump to them later.

_This is now deprecated in favour of [go-west](https://github.com/bashmarklets/go-west) which is a pure bash (and zsh) implementation of the go script, specifically rewritten to avoid dependencies on Tcl or any other scripting languages. It also has more features and better tab completion._

## Installation

Ensure [Tcl8.5](https://www.tcl.tk/) and [tcllib](http://tcllib.sourceforge.net/) are installed, grab Go from the [download](https://bitbucket.org/bakkeby/go/downloads) section and unpack it at your preferred location.

Then to use the "go" script add the following to your .bashrc and replace */path/to/go* with your installation path:

```
#!bash
go () {
        _GO_DIR=/path/to/go
        if [ $(expr match "$1" '--\?[^-p].*') != 0 ]; then
                tclsh $_GO_DIR/go.tcl $@
        elif [ "$1" = "" ]; then
                tclsh $_GO_DIR/go.tcl $@
        else
                _CMD=$(tclsh $_GO_DIR/go.tcl $@)
                if [[ "$_CMD" != "" ]]; then
                        eval $_CMD
                        ls -a --color=auto
                fi
                unset _CMD
        fi
        unset _GO_DIR
}
```

Note: you don't have to name the bash function "go", you can name it whatever you want (even "cd").

Also feel free to remove the "*ls -a --color=auto*" line if you don't like the auto listing of directory files.

## Example usage
```
#!bash
$ go --help
Usage: go [OPTION?] [DIRECTORY?] [BOOKMARK?]

  -a, --add                  adds directory with the given bookmark
  -l, --list                 lists current bookmarks and directories
  -r, --remove               removes a given bookmark
      --clear                removes all bookmarks
  -h, --help                 display this help message

Predefined bookmarks:
  next                       moves to directory adjacent to current directory
  prev                       moves to directory adjacent to current directory
  first                      moves to first adjacent directory
  last                       moves to last adjacent directory
  up                         moves up a directory
  down                       moves down into first directory
  home                       moves to home directory
  ...                        moves up two directories
  ....                       moves up three directories

Examples:
  go --add . music           adds current directory with the bookmark "music"
  go -l                      lists currently stored directories
  go music                   changes the current directory to "music"
  go -r music                removes the bookmark with the key "music"
  
```

## Also see these related projects

   - http://www.huyng.com/projects/bashmarks/
   - http://www.skamphausen.de/cgi-bin/ska/CDargs
   - https://github.com/bulletmark/cdhist
   - https://github.com/karlin/working-directory
   - http://micans.org/apparix/
   - https://github.com/wting/autojump/wiki
   - https://github.com/frazenshtein/fastcd
   - https://github.com/jeroenvisser101/project-switcher
   - http://xd-home.sourceforge.net/xdman.html
   - https://github.com/rupa/z/
   - https://github.com/perlino/scripts/blob/master/acd_func.sh
   - http://manpages.ubuntu.com/manpages/hardy/man7/wcd.7.html

-----

# DBStore

DBStore is a Tcl tool to store arbitrary key value pairs of information for lookup purposes. The name is not brilliant, I know.

The idea is to have a relatively simple way of remembering pieces of information between script runs and it's also a plus if it is platform agnostic.

The way I use this so far is to allow scripts to remember when they last ran and also store the a checksum (MD5 or otherwise) of specific directories so that a backup script knows if a directory has changed since the last run.

## Installation

Ensure [Tcl8.5](https://www.tcl.tk/) and [tcllib](http://tcllib.sourceforge.net/) are installed, grab Go from the [download](https://bitbucket.org/bakkeby/go/downloads) section and unpack it at your preferred location.

## Example usage

```
#!bash
$ dbstore --help
Usage: dbstore [OPTION?] [VALUE?] [KEY?]
Stores arbitrary key-value pairs for lookup purposes.

  -a, --add                  adds a value with the given key
  -l, --list                 lists current keys and values
      --locate               list location of dbstore data file
  -r, --remove               removes a given key
      --clear                removes all keys
  -h, --help                 display this help message

Examples:
    dbstore --add value key  stores "value" with the key "key"
    dbstore -l               lists currently stored keys and values
    dbstore key              returns the value associated with the key "key"
    dbstore -r key           removes the value assicated with the key "key"

```

-----

## License

   - Simplified BSD License/FreeBSD License (GPL-compatible free software license)

-----

## Dependencies

   - [TApp](https://bitbucket.org/bakkeby/tapp)
   - [Tcl8.5](https://www.tcl.tk/)
   - [tcllib](http://tcllib.sourceforge.net/)