# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script] [file join [file dirname [file dirname $script]] tapp]

package require tapp 1.0
package provide array_store_handler 1.0

namespace eval array_store_handler {}

proc array_store_handler::init {} {
	set pname [opt::proc array_store --preset array]
	array_store init [cfg get ARRAY_STORE_FILE]
	array_store load
	
	param register {} --clear\
		--body {
			array_store remove *
		}\
		--comment {removes all keys}

	param register {} --locate\
		--body {
			log STDOUT {[array_store location]}
		}\
		--comment {list location of data file}
	
	param register -l --list\
		--body {
			set print_format [cfg get ARRAY_STORE_PRINT_FORMAT {%-29s %s}]
			log STDOUT [format $print_format Key Value]
			log STDOUT [string repeat "-" [cfg get ARRAY_STORE_PRINT_RULER_LENGTH 80]]
			foreach key [lsort [array_store keys]] {
				log STDOUT [format $print_format $key [array_store get $key]]
			}
			log STDOUT {\r}
		}\
		--comment {lists current keys and values}
		
	param register -a --add\
		--body {
			lassign $values value key
			if {$key == {}} {
				set error_msg {Key required to add data to array store (value = "$value")}
				log ERROR $error_msg
				log STDOUT $error_msg
			} else {
				array_store set $key $value
				log STDOUT {$value added as $key}
			}
		}\
		--max_args 2\
		--min_args 1\
		--comment {adds a value with the given key}
		
	param register -k --keys\
		--body {
			log STDOUT {[lsort [array_store keys]]}
		}\
		--comment {lists current keys}
	
	param register -r --remove\
		--body {
			if {![array_store exists $value]} {
				log STDOUT {Key '$value' does not exist}
			} else {
				set data [array_store get $value]
				array_store remove $value
				log STDOUT {Key '$value' referring to $data removed}
			}
		}\
		--max_args 1\
		--min_args 1\
		--comment {removes a given key}
}

proc array_store_handler::main { argv } {

	if {[llength $argv] < 1} {
		set argv --help
	}
	
	log DEBUG {Parsing array store package arguments}
	
	# Treat each remaining argument following argument parsing
	# as keys to look up
	foreach key [param parse $argv] {
		lookup $key
	}
	
	array_store save
}

# Returns the value associated with the given key (if any).
proc array_store_handler::lookup {key} {
	log STDOUT {[array_store get $key]}
}
