#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# APPTAG|tcl|dbstore|Tool to store arbitrary keys and values for lookup purposes
#
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require dbstore 1.0

cfg set HELP \
{Usage: dbstore [OPTION?] [VALUE?] [KEY?]
Stores arbitrary key-value pairs for lookup purposes.

%args%
Examples:
    dbstore --add value key  stores "value" with the key "key"
    dbstore -l               lists currently stored keys and values
    dbstore key              returns the value associated with the key "key"
    dbstore -r key           removes the value assicated with the key "key"

Warning! Please return any lost keys to the reception.
}

cfg file [file rootname [this]].cfg

if {[info exists argv]} {
	dbstore::main $argv
}