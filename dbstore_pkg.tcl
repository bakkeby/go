# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0
package require array_store_handler 1.0

package provide dbstore 1.0

namespace eval dbstore {}

proc dbstore::write_default_config { file } {
	file_utils::write_file $file {
LOG_FILE = stdout
LOG_LEVEL = INFO
ARRAY_STORE_FILE = $::env(HOME)/dbstore.db
ARRAY_STORE_PRINT_FORMAT = %-29s %s
ARRAY_STORE_PRINT_RULER_LENGTH = 80 
}
}

proc dbstore::main { argv } {
	array_store_handler::init
	array_store_handler::main $argv
}
