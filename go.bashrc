# Copyright (c) 2015, Stein Gunnar Bakkeby, all rights reserved.
# This file is to be sourced in your .bashrc or .bash_profile

# APPTAG|bash|go|Bookmark directories to quickly cd into them later
go () {
	local _GO_DIR _CMD
	_GO_DIR=~/bin/go
	if [ $(expr match "$1" '--\?[^-p].*') != 0 ]; then
		tclsh $_GO_DIR/go.tcl "$@"
	elif [ "$1" = "" ]; then
		tclsh $_GO_DIR/go.tcl "$@"
	else
		_CMD=$(tclsh $_GO_DIR/go.tcl "$@")
		if [ "$_CMD" != "" ]; then
			eval "$_CMD"
		else
			_CMD=$(morg --find "$@" --path | head -n 1)
			if [ "$_CMD" != "" ]; then
				if [ -e "$_CMD" ]; then
					cd "$_CMD"
					ls -a --color=auto
				fi
			fi
		fi
	fi
}

if [ $ZSH_VERSION ]; then
	function _gocomp {
		reply=($(go -k))
	}
	compctl -K _gocomp go
else
	function _gocomp {
		COMPREPLY=($(compgen -W '$(go -k)' -- ${COMP_WORDS[COMP_CWORD]}))
		return 0
	}
	shopt -s progcomp
	complete -F _gocomp go
fi
