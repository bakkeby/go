# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
#
# To run this script, save it to ~/bin/go and add the following to your .bashrc:
#
# source ~/bin/go/go.bashrc
#
# Alternatively you can either:
#   - create a symlink ~/bin/go pointing to the go folder or
#   - update the path in the go.bashrc6 file to point to the right folder
#
# Calling this directly from the command line serves little purpose as it won't
# allow the directory to be changed on the command line itself. This is because
# the "cd" command is internal to the shell and changing the directory in a
# script just changes it for the given sub-process, not the current one.
#
# It would be possible to name the function "cd" as we specifically address the
# builtin cd command.
#

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require go 1.0

cfg set HELP \
{Usage: go [OPTION?] [DIRECTORY?] [BOOKMARK?]

%args%
Predefined bookmarks:
  next                       moves to directory adjacent to current directory
  prev                       moves to directory adjacent to current directory
  first                      moves to first adjacent directory
  last                       moves to last adjacent directory
  up                         moves up a directory
  down                       moves down into first directory
  home                       moves to home directory
  ...                        moves up two directories
  ....                       moves up three directories

Examples:
  go --add . music           adds current directory with the bookmark "music"
  go -l                      lists currently stored directories
  go music                   changes the current directory to "music"
  go -r music                removes the bookmark with the key "music"

Warning! Violent fish. Do not go outside the boundaries of your imagination.
}

cfg lappend ARGS \
	{-a --add}              {adds directory with the given bookmark}\
	{-l --list}             {lists current bookmarks and directories}\
	{-r --remove}           {removes a given bookmark}\
	{--clear}               {removes all bookmarks}\
	{-h --help}             {display this help message}

if {[info exists argv]} {
	cfg file [file rootname [this]].cfg
	go::main $argv
}
