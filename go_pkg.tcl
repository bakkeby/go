# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package require array_store_handler 1.0

package provide go 1.0

namespace eval go {}
namespace eval array_store_handler {}

proc go::write_default_config { file } {
	file_utils::write_file $file {
LOG_FILE = stdout
LOG_LEVEL = STDOUT
ARRAY_STORE_FILE = $::env(HOME)/.go.db
ARRAY_STORE_PRINT_FORMAT = \ \ \ \ %-20s%-10s
AUTO_LS_ON_DIR_CHANGE = 1
}
}

# Overrides parameters that are otherwise handled in the array store handler
proc go::init {} {
	param register -a --add\
		--override\
		--body {
			lassign $values value key
			if {[file exists $value]} {
				set value [file normalize $value]
				if {$key == {}} {
					set key [string tolower [file tail $value]]
				}
			}
			
			if {$key == {}} {
				set error_msg {Key required to add bookmark to go (value = "$value")}
				log ERROR $error_msg
				log STDOUT $error_msg
			} else {
				array_store_handler::array_store set $key $value
				log STDOUT {$value added as $key}
			}
		}\
		--max_args 2\
		--min_args 1\
		--comment {adds a value with the given key}
}

# Custom lookup handler, overrides the procedure in the array store handler.
proc array_store_handler::lookup {key} {
	set location [array_store get $key]
	if {$location != {}} {
		# do nothing, we have a stored bookmark
	} elseif {[file exists $key] && [file isdirectory $key]} {
		set location $key
	} elseif {[lsearch {
			next
			prev
			previous
			up
			down
			first
			last
			-
			home
		} $key] >= 0} {
		set location [get_relative_location $key]
	} elseif {[regexp {^[.]{3,}$} $key]} {
		set location [string repeat {../} [expr {[string length $key] - 1}]]
	}

	if {$location != {}} {
		if {$location == {-} || [file exists $location]} {
			if {[pwd] != $location} {
				log STDOUT {builtin cd $location}
				if {[cfg enabled AUTO_LS_ON_DIR_CHANGE 1]} {
					log STDOUT {[cfg get AUTO_LS_COMMAND {ls -a --color=auto}]}
				}
			}
		} else {
			log STDOUT {echo $location}
			log STDOUT {$location}
		}
	}
}

proc get_relative_location { key } {

	switch -- $key {
	up     { return .. }
	-      { return - }
	home   { return $::env(HOME) }
	}

	if {$key == {down}} {
		set directory .
	} else {
		set directory ..
	}

	set current [file tail [pwd]]
	set directories [lsort -nocase [glob -nocomplain -types d -tails -directory $directory *]]
	log DEBUG {Current is $current and directories are $directories}

	set index [lsearch $directories $current]
	switch -- $key {
	next {
		incr index
	}
	previous -
	prev {
		incr index -1
	}
	down -
	first {
		set index 0
	}
	last {
		set index [expr {[llength $directories] - 1}]
	}
	}

	set dir [lindex $directories $index]
	if {$dir != {}} {
		return $directory/$dir
	}

	return
}

proc go::main { argv } {
	array_store_handler::init
	go::init
	array_store_handler::main $argv
}
