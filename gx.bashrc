# Copyright (c) 2015, Stein Gunnar Bakkeby, all rights reserved.
# This file is to be sourced in your .bashrc or .bash_profile

# APPTAG|bash|gx|Bookmark often used commands to quickly run them later
gx () {
	local _GX_DIR _CMD
	_GX_DIR=~/bin/go
	if [ $(expr match "$1" '--\?[^-p].*') != 0 ]; then
		tclsh $_GX_DIR/gx.tcl "$@"
	elif [ "$1" = "" ]; then
		tclsh $_GX_DIR/gx.tcl "$@"
	else
		_CMD=$(tclsh $_GX_DIR/gx.tcl "$@")
		eval "$_CMD"
	fi
}

if [ $ZSH_VERSION ]; then
	function _gxcomp {
		reply=($(gx -k))
	}
	compctl -K _gxcomp gx
else
	function _gxcomp {
		COMPREPLY=($(compgen -W '$(gx -k)' -- ${COMP_WORDS[COMP_CWORD]}))
		return 0
	}
	shopt -s progcomp
	complete -F _gxcomp gx
fi
