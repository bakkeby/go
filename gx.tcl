# Copyright (c) 2015, Stein Gunnar Bakkeby, all rights reserved.
#
# To run this script, save it to ~/bin/go and add the following to your .bashrc:
#
# source ~/bin/go/gx.bashrc
#
# Alternatively you can either:
#   - create a symlink ~/bin/go pointing to the go folder or
#   - update the path in the gx.bashrc file to point to the right folder
#

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require gx 1.0

cfg set HELP \
{Usage: gx [OPTION]... [BOOKMARK?]

Bookmark often used commands and execute them later on the fly.

Optional arguments:
%args%
(arguments are not dependent on which order they come in)

Examples:
  gx -a "ls -l ~/" lshome    adds the command with the bookmark "lshome"
  gx -l                      lists currently stored commands
  gx lshome                  executes the command stored as "lshome"
  gx -r lshome               removes the bookmarked command with the key "lshome"

Warning! Do not place this product into any electronic equipment. 
}

cfg lappend ARGS \
	{-h --help}             {display this help}

if {[info exists argv]} {
	cfg file [file rootname [this]].cfg
	gx::main $argv
}
