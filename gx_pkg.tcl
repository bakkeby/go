# Copyright (c) 2015, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package require array_store_handler 1.0

package provide gx 1.0

namespace eval gx {}
namespace eval array_store_handler {}

proc gx::write_default_config { file } {
	file_utils::write_file $file {
LOG_FILE = stdout
LOG_LEVEL = STDOUT
ARRAY_STORE_FILE = $::env(HOME)/.gx.db
ARRAY_STORE_PRINT_FORMAT = \ \ \ \ %-20s%-10s
}
}

# Custom lookup handler, overrides the procedure in the array store handler.
proc array_store_handler::lookup {key} {
	log STDOUT {echo "[array_store get $key]"}
	if {[cfg disabled DRY_RUN]} {
		log STDOUT [array_store get $key]
	}
}

proc gx::main { argv } {
	array_store_handler::init
	set keys [param parse $argv]
	if {[llength $keys] > 0} {
		array_store_handler::lookup [join $keys]
	}
	array_store_handler::array_store save
}


